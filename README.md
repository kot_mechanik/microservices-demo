# microservices-demo

Вывод команд:

```console
[mechanik@kubereduc ~]$ kubectl get canaries -n microservices-demo
NAME       STATUS      WEIGHT   LASTTRANSITIONTIME
frontend   Succeeded   0        2022-07-27T19:40:43Z
```

```console
[mechanik@kubereduc ~]$
[mechanik@kubereduc ~]$
[mechanik@kubereduc ~]$ kubectl describe canary -n microservices-demo frontend
Name:         frontend
Namespace:    microservices-demo
Labels:       app.kubernetes.io/managed-by=Helm
Annotations:  helm.fluxcd.io/antecedent: microservices-demo:helmrelease/frontend
              meta.helm.sh/release-name: frontend
              meta.helm.sh/release-namespace: microservices-demo
API Version:  flagger.app/v1beta1
Kind:         Canary
Metadata:
  Creation Timestamp:  2022-07-27T19:21:53Z
  Generation:          1
  Managed Fields:
    API Version:  flagger.app/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          .:
          f:meta.helm.sh/release-name:
          f:meta.helm.sh/release-namespace:
        f:labels:
          .:
          f:app.kubernetes.io/managed-by:
      f:spec:
        .:
        f:analysis:
          .:
          f:interval:
          f:iterations:
          f:maxWeight:
          f:threshold:
        f:provider:
        f:service:
          .:
          f:gateways:
          f:hosts:
          f:port:
          f:targetPort:
          f:trafficPolicy:
            .:
            f:tls:
              .:
              f:mode:
        f:targetRef:
          .:
          f:apiVersion:
          f:kind:
          f:name:
    Manager:      helm-operator
    Operation:    Update
    Time:         2022-07-27T19:21:53Z
    API Version:  flagger.app/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:metadata:
        f:annotations:
          f:helm.fluxcd.io/antecedent:
    Manager:      kubectl
    Operation:    Update
    Time:         2022-07-27T19:21:53Z
    API Version:  flagger.app/v1beta1
    Fields Type:  FieldsV1
    fieldsV1:
      f:spec:
        f:service:
          f:portDiscovery:
      f:status:
        .:
        f:canaryWeight:
        f:conditions:
        f:failedChecks:
        f:iterations:
        f:lastAppliedSpec:
        f:lastPromotedSpec:
        f:lastTransitionTime:
        f:phase:
        f:trackedConfigs:
    Manager:         flagger
    Operation:       Update
    Time:            2022-07-27T19:31:44Z
  Resource Version:  20159
  UID:               98c9df2c-dc00-4925-ba72-4e491c3a60ba
Spec:
  Analysis:
    Interval:    30s
    Iterations:  2
    Max Weight:  30
    Threshold:   1
  Provider:      istio
  Service:
    Gateways:
      frontend
    Hosts:
      *
    Port:         80
    Target Port:  8080
    Traffic Policy:
      Tls:
        Mode:  DISABLE
  Target Ref:
    API Version:  apps/v1
    Kind:         Deployment
    Name:         frontend
Status:
  Canary Weight:  0
  Conditions:
    Last Transition Time:  2022-07-27T19:40:43Z
    Last Update Time:      2022-07-27T19:40:43Z
    Message:               Canary analysis completed successfully, promotion finished.
    Reason:                Succeeded
    Status:                True
    Type:                  Promoted
  Failed Checks:           0
  Iterations:              0
  Last Applied Spec:       648cbc4694
  Last Promoted Spec:      648cbc4694
  Last Transition Time:    2022-07-27T19:40:43Z
  Phase:                   Succeeded
  Tracked Configs:
Events:
  Type     Reason  Age                From     Message
  ----     ------  ----               ----     -------
  Warning  Synced  35m                flagger  frontend-primary.microservices-demo not ready: waiting for rollout to finish: observed deployment generation less than desired generation
  Normal   Synced  34m (x2 over 35m)  flagger  all the metrics providers are available!
  Normal   Synced  34m                flagger  Initialization done! frontend.microservices-demo
  Normal   Synced  29m                flagger  New revision detected! Scaling up frontend.microservices-demo
  Warning  Synced  28m                flagger  frontend-primary.microservices-demo not ready: waiting for rollout to finish: 0 of 1 (readyThreshold 100%) updated replicas are available
  Normal   Synced  28m                flagger  Starting canary analysis for frontend.microservices-demo
  Normal   Synced  28m                flagger  Advance frontend.microservices-demo canary iteration 1/2
  Normal   Synced  27m                flagger  Advance frontend.microservices-demo canary iteration 2/2
  Normal   Synced  27m                flagger  Routing all traffic to canary
  Normal   Synced  26m                flagger  Copying frontend.microservices-demo template spec to frontend-primary.microservices-demo
  Normal   Synced  26m                flagger  Routing all traffic to primary
  Normal   Synced  25m                flagger  (combined from similar events): Promotion completed! Scaling down frontend.microservices-demo
[mechanik@kubereduc ~]$
```
